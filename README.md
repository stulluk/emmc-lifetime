# emmc-lifetime

This is a small tool to read extcsd register of emmc devices. 

# Building

gcc -o emmc-lifetime emmc-lifetime.c

# Running

./emmc-lifetime /dev/mmcblk0 

or

./emmc-lifetime /dev/mmcblk1 -v

( Your emmc device may be at path /dev/mmcblk1 or 2, check your hardware !!! )

![tty.gif](tty.gif)

# Detailed Information

Especially, 269th byte of extcsd provides EXT_CSD_DEVICE_LIFE_TIME_EST_TYP_B, see below tables.

The Extended CSD register defines the e·MMC properties and selected modes. It is 512 bytes long.
The most significant 320 bytes are the Properties segment, which defines the e·MMC capabilities and cannot be modified by the host. The lower 192
bytes are the Modes segment, which defines the configuration the e·MMC is working in. These modes can be changed by the host by means of the
SWITCH command.

## Lifetime Related Fields of EXTCSD Register

| EXTCSD Byte Number | Field Name                 | Description                                                                                                                                                |
|--------------------|----------------------------|------------------------------------------------------------------------------------------------------------------------------------------------------------|
| 267                | PRE_EOL_INFO               | Indicates the device lifetime reflected by average reserved blocks                                                                                         |
| 268                | DEVICE_LIFE_TIME_EST_TYP_A | Indicates the estimation of the device lifetime that is reflected by the averaged wear out of SLC areas relative to its maximum estimated device lifetime  |
| 269                | DEVICE_LIFE_TIME_EST_TYP_B | Indicates the estimation of the device lifetime which is reflected by the averaged wear out of MLC areas relative to its maximum estimated device lifetime |


Mapping values of EXT_CSD_DEVICE_LIFE_TIME_EST_TYP_B and EXT_CSD_DEVICE_LIFE_TIME_EST_TYP_A

| Value   | Description                                     |
|---------|-------------------------------------------------|
| 0x00    | Not defined                                     |
| 0x01    | 0% - 10% device life time used                  |
| 0x02    | 10% -20% device life time used                  |
| 0x03    | 20% -30% device life time used                  |
| 0x04    | 30% - 40% device life time used                 |
| 0x05    | 40% - 50% device life time used                 |
| 0x06    | 50% - 60% device life time used                 |
| 0x07    | 60% - 70% device life time used                 |
| 0x08    | 70% - 80% device life time used                 |
| 0x09    | 80% - 90% device life time used                 |
| 0x0A    | 90% - 100% device life time used                |
| 0x0B    | Exceeded its maximum estimated device life time |
| Others  | Reserved                                        |


PRE_EOL_INFO value mapping

| Value       | Pre-EOL Info. | Description                    |
|-------------|---------------|--------------------------------|
| 0x00        | Not defined   |                                |
| 0x01        | Normal        | Normal                         |
| 0x02        | Warning       | Consumed 80% of reserved block |
| 0x03        | Urgent        |                                |
| 0x04 ~ 0xFF | Reserved      |                                |
